﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using manageruser.Models.Dao;
using manageruser.Models.Entity;
using manageruser.Models.Logic;
using manageruser.Models.Utils;
using MySql.Data.MySqlClient;

namespace manageruser.Models.Validates
{
    /// <summary>
    /// Xử lý validate 
    /// Create by NgaPLT 21/12/2019
    /// </summary>
    public class UserValidate
    {
        TblUserLogic tblUserLogic = new TblUserLogic();
        MstGroupLogic mstGroupLogic = new MstGroupLogic();
        MstJapanLogic mstJapanLogic = new MstJapanLogic();
        /// <summary>
        /// Check lohinName
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <param name="loginName"></param>
        /// <returns></returns>
        public string CheckLoginName(string loginName)
        {
            string err = null;
            if (string.IsNullOrEmpty(loginName))
            {
                err = ConstansErr.ERR001_LOGIN_NAME;
            }
            else if (!Common.CheckLength(loginName, ConstansFormat.MAX_CHAR, ConstansFormat.MIN_CHAR_LOGIN))
            {
                err = ConstansErr.ERR007_LOGIN_NAME;
            }
            else if (!Common.CheckFormat(loginName, ConstansFormat.FORMAT_LOGIN_NAME))
            {
                err = ConstansErr.ERR019_LOGIN_NAME;
            }
            else if (tblUserLogic.CheckExitLogin(loginName))
            {
                err = ConstansErr.ERR003_LOGIN_NAME;
            }
            return err;
        }
        /// <summary>
        /// Check GroupId
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public string CheckGroup(int groupId)
        {
            string err = null;
            if (groupId == 0)
            {
                err = ConstansErr.ERR002_GROUP;
            }
            else if (!mstGroupLogic.CheckGroupById(groupId))
            {
                err = ConstansErr.ERR004_GROUP;
            }
            return err;
        }
        /// <summary>
        /// Check fullName
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public static String CheckFullName(String fullName)
        {
            String error = null;
            if (string.IsNullOrEmpty(fullName))
            {
                error = ConstansErr.ERR001_FULL_NAME;
            }
            else if (!Common.CheckLength(fullName, ConstansFormat.MAX_LENGTH, ConstansFormat.MIN_LENGTH))
            {
                error = ConstansErr.ERR006_FULL_NAME;
            }
            return error;
        }
        /// <summary>
        /// Check full name kana
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <param name="fullNameKana"></param>
        /// <returns></returns>
        private string CheckFullNameKana(String fullNameKana)
        {
            String error = null;
            if (!String.IsNullOrEmpty(fullNameKana))
            {
                if (!Common.CheckLength(fullNameKana, ConstansFormat.MAX_LENGTH, ConstansFormat.MIN_LENGTH))
                {
                    error = ConstansErr.ERR006_FULL_NAME_KANA;
                }
                else if (!Common.CheckFormat(fullNameKana, ConstansFormat.FORMAT_KANA))
                {
                    error = ConstansErr.ERR009_FULL_NAME_KANA;
                }
            }
            return error;
        }
        /// <summary>
        /// Check email
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <param name="email"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        private string CheckEmail(String email, int userId)
        {
            String error = null;

            if (String.IsNullOrEmpty(email))
            {
                error = ConstansErr.ERR001_EMAIL;
            }
            else if (!Common.CheckLength(email, ConstansFormat.MAX_MAIL, ConstansFormat.MIN_LENGTH))
            {// Kiểm tra maxlength
                error = ConstansErr.ERR006_EMAIL;
            }
            else if (!Common.CheckFormat(email, ConstansFormat.FORMAT_EMAIL))
            {
                error = ConstansErr.ERR011_EMAIL;

            }
            else if (tblUserLogic.CheckExitEmail(email, userId))
            {

                error = ConstansErr.ERR003_EMAIL;
            }
            return error;
        }
        /// <summary>
        /// check tel
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <param name="tel"></param>
        /// <returns></returns>
        private string CheckTel(String tel)
        {
            String error = null;

            if (String.IsNullOrEmpty(tel))
            {
                error = ConstansErr.ERR001_TEL;
            }
            else if (!Common.CheckLength(tel, ConstansFormat.MAX_TEL, ConstansFormat.MIN_CHAR))
            {// Kiểm tra maxlength
                error = ConstansErr.ERR006_TEL;
            }
            else if (!Common.CheckFormat(tel, ConstansFormat.FORMAT_TEL))
            {
                error = ConstansErr.ERR011_TEL;
            }
            return error;
        }
        /// <summary>
        /// check password
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <param name="password"></param>
        /// <param name="passwordConfirm"></param>
        /// <returns></returns>
        public string CheckPassword(String password, String passwordConfirm)
        {
            String error = null;
            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMiniMaxChars = new Regex(@".{8,15}");
            var hasLowerChar = new Regex(@"[a-z]+");
            var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");
            if (String.IsNullOrEmpty(password))
            {
                error = ConstansErr.ERR001_PASSWORD;
            }
            else if (!hasLowerChar.IsMatch(password))
            {
                error = ConstansErr.ERR0014_PASSWORD_HAS_LOWER_CHAR;
            }
            else if (!hasMiniMaxChars.IsMatch(password))
            {
                error = ConstansErr.ERR0013_PASSWORD_HAS_MINIMAX_CHARS;
            }
            else if (!hasNumber.IsMatch(password))
            {
                error = ConstansErr.ERR0011_PASSWORD_HAS_NUMBER;
            }
            else if (!hasSymbols.IsMatch(password))
            {
                error = ConstansErr.ERR0015_PASSWORD_HAS_SYMBOLS;
            }
            else if (!hasUpperChar.IsMatch(password))
            {
                error = ConstansErr.ERR0012_PASSWORD_HAS_UPPER_CHAR;
            }
            else if (!Common.CheckLength(password, ConstansFormat.MAX_CHAR, ConstansFormat.MIN_CHAR))
            {
                error = ConstansErr.ERR007_PASSWORD;
            }
            else if (!Common.CheckByte(password))
            {
                error = ConstansErr.ERR008_PASSWORD;

            }
            else if (!password.Equals(passwordConfirm))
            {
                error = ConstansErr.ERR017_PASSWORDCONFIRM;
            }
            return error;
        }
        /// <summary>
        /// Check birthday
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public string CheckBirthday(int year, int month, int day)
        {
            String error = null;

            if (!Common.CheckFormatDate(year, month, day))
            {
                error = ConstansErr.ERR011_BIRTHDAY;
            }
            return error;
        }
        /// <summary>
        /// check trình độ tiếng nhật
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public string CheckJapan(string codeLevel)
        {
            String error = null;
            if (!mstJapanLogic.CheckJapanById(codeLevel))
            {
                error = ConstansErr.ERR004_CODE_LEVEL;
            }
            return error;
        }
        /// <summary>
        /// check ngày bắt đầu
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public string CheckStart(int year, int month, int day)
        {
            String error = null;
            if (!Common.CheckFormatDate(year, month, day))
            {
                error = ConstansErr.ERR011_START_DATE;
            }
            return error;
        }
        /// <summary>
        /// Check ngày kế thúc
        /// create by NgaPLT 21/12/2019
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public string CheckEnd(int startY, int startM, int startD, int endY, int endM, int endD)
        {
            String error = null;
            if (!Common.CheckFormatDate(startY, startM, startD))
            {
                error = ConstansErr.ERR011_END_DATE;
            }
            else if (!Common.CheckEndDate(startY, startM, startD, endY, endM, endD))
            {
                error = ConstansErr.ERR012_END_DATE;
            }
            return error;
        }
        /// <summary>
        /// Check total
        /// Create by NgaPLT 21/12/2019 
        /// </summary>
        /// <param name="total"></param>
        /// <returns></returns>
        public string CheckTotal(string total)
        {
            String error = null;
            if (String.IsNullOrEmpty(total))
            {
                error = ConstansErr.ERR001_TOTAL;
            }
            else if (!Common.CheckFormat(total, ConstansFormat.FORMAT_TOTAL))
            {
                error = ConstansErr.ERR018_TOTAL;
            }
            return error;
        }
        /// <summary>
        /// Kiểm tra các giá trị đã nhập userInfo
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        public List<string> ValidateUserInfo(UserInfo userInfo)
        {
            List<string> listErr = new List<string>();
            if (userInfo.UserId == 0)
            {
                listErr = Common.AddErr(listErr, CheckLoginName(userInfo.LoginName));
            }
            listErr = Common.AddErr(listErr, CheckGroup(userInfo.GroupId));
            listErr = Common.AddErr(listErr, CheckFullName(userInfo.FullName));
            listErr = Common.AddErr(listErr, CheckFullNameKana(userInfo.FullNameKana));
            listErr = Common.AddErr(listErr, CheckBirthday(userInfo.BirthYear, userInfo.BirthMonth, userInfo.BirthDay));
            listErr = Common.AddErr(listErr, CheckEmail(userInfo.Email, userInfo.UserId));
            listErr = Common.AddErr(listErr, CheckTel(userInfo.Tel));
            if (userInfo.UserId == 0)
            {
                listErr = Common.AddErr(listErr, CheckPassword(userInfo.Password, userInfo.PasswordConfirm));
            }
            if (userInfo.CodeLevel != "0" && !string.IsNullOrEmpty(userInfo.CodeLevel))
            {
                listErr = Common.AddErr(listErr, CheckJapan(userInfo.CodeLevel));
                listErr = Common.AddErr(listErr, CheckStart(userInfo.StartYear, userInfo.StartMonth, userInfo.StartDay));
                listErr = Common.AddErr(listErr, CheckEnd(userInfo.StartYear, userInfo.StartMonth, userInfo.StartDay, userInfo.EndYear, userInfo.EndMonth, userInfo.EndDay));
                listErr = Common.AddErr(listErr, CheckTotal(userInfo.Total));
            }
            return listErr;
        }
        /// <summary>
        /// insertUserInfo
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        public Boolean InsertUserInfo(UserInfo userInfo)
        {
            // Tạo biến boolean lưu kết quả thực hiện
            Boolean resultInsert = true;

            TblDetailUserJapanDao tblDetailUserJapanDao = new TblDetailUserJapanDao();

            // Mở kết nối chung.
            TblUserDao tblUserDao = new TblUserDao();
            tblUserDao.OpenConnection();
            MySqlConnection con = tblUserDao.conn;
            // Khởi tạo đối tượng Transaction
            MySqlTransaction transaction;
            transaction = con.BeginTransaction();
            try
            {

                // Insert thông tin vào bảng tblUser
                int result = tblUserDao.InsertTblUser(userInfo, con, transaction);
                if (result != -1 )
                {
                    if (!"0".Equals(userInfo.CodeLevel) && !string.IsNullOrEmpty(userInfo.CodeLevel))
                    {
                        userInfo.UserId = result;
                        resultInsert = tblDetailUserJapanDao.InsertDetailUserJapan(userInfo, con, transaction);
                        userInfo.UserId = 0;
                    }
                }
                else
                {
                    resultInsert = false;
                }
                // Thực hiện commit
                transaction.Commit();
            }
            catch (MySqlException e)
            {
                transaction.Rollback();
                throw e;
            }
            finally
            {
                // Đóng kết nối với DB.
                tblUserDao.CloseConnection();
            }
            return resultInsert;
        }
        /// <summary>
        /// updateUserInfo
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        public Boolean UpdateUserInfo(UserInfo userInfo)
        {
            // Tạo biến boolean lưu kết quả thực hiện
            Boolean resultInsert = false;

            TblDetailUserJapanDao tblDetailUserJapanDao = new TblDetailUserJapanDao();

            // Mở kết nối chung.
            TblUserDao tblUserDao = new TblUserDao();
            tblUserDao.OpenConnection();
            MySqlConnection con = tblUserDao.conn;
            // Khởi tạo đối tượng Transaction
            MySqlTransaction transaction;
            transaction = con.BeginTransaction();
            try
            {
                // Insert thông tin vào bảng tblUser
                resultInsert = tblUserDao.UpdateTblUser(userInfo, con, transaction);
                if (resultInsert)
                {
                    // Kiểm tra trình độ tiêng nhật của User.
                    Boolean exitStatus = tblDetailUserJapanDao.CheckExitsLevelJapanUser(userInfo.UserId);

                    if (exitStatus)
                    {
                        if (!"0".Equals(userInfo.CodeLevel) && !string.IsNullOrEmpty(userInfo.CodeLevel))
                        {
                            //updare
                            resultInsert = tblDetailUserJapanDao.UpdateTblDetailUserJapan(userInfo, con, transaction);
                        }
                        else
                        {
                            // xoa
                            resultInsert = tblDetailUserJapanDao.DeleteTblDetailUserJapan(userInfo.UserId, con, transaction);

                        }
                    }
                    else if (!"0".Equals(userInfo.CodeLevel) && !string.IsNullOrEmpty(userInfo.CodeLevel))
                    {
                        // insert
                        resultInsert = tblDetailUserJapanDao.InsertDetailUserJapan(userInfo, con, transaction);
                    }
                }
                // Thực hiện commit
                transaction.Commit();
            }
            catch (MySqlException e)
            {
                transaction.Rollback();
                throw e;
            }
            finally
            {
                // Đóng kết nối với DB.
                tblUserDao.CloseConnection();
            }
            return resultInsert;
        }
        /// <summary>
        /// Delete userInfo
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public Boolean DeleteserInfo(int userID)
        {
            // Tạo biến boolean lưu kết quả thực hiện
            Boolean resultInsert = true;
            TblUserDao tblUserDao = new TblUserDao();
            TblDetailUserJapanDao tblDetailUserJapanDao = new TblDetailUserJapanDao();
            tblUserDao.OpenConnection();
            MySqlConnection con = tblUserDao.conn;
            // Khởi tạo đối tượng Transaction
            MySqlTransaction transaction;
            transaction = con.BeginTransaction();
            try
            {
              

               
                // Kiểm tra trình độ tiêng nhật của User.
                Boolean exitStatus = tblDetailUserJapanDao.CheckExitsLevelJapanUser(userID);

                if ( exitStatus)
                {
                    resultInsert = tblDetailUserJapanDao.DeleteTblDetailUserJapan(userID, con, transaction);
                }
                if (resultInsert)
                {
                    resultInsert = tblUserDao.DeleteTblUser(userID, con, transaction);
                }

                // Thực hiện commit
                transaction.Commit();
            }
            catch (MySqlException e)
            {
                transaction.Rollback();
                Console.WriteLine(e);
                throw e;
            }
            finally
            {
                // Đóng kết nối với DB.
                tblUserDao.CloseConnection();
            }
            return resultInsert;
        }
    }
    
}