﻿using manageruser.Models.Entity;
using MySql.Data.MySqlClient;
using System;
using System.Text;

namespace manageruser.Models.Dao
{
    /// <summary>
    /// Class thao tác tới bảng tblDetailUserJapan
    /// Create by NGaPLT
    /// </summary>
    public class TblDetailUserJapanDao : BaseDao
    {
        /// <summary>
        /// Kiểm tra trình đọ tiếng nhật theo userId
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>nếu tồn tại trả về true, ngược lại trả về false</returns>
        public Boolean CheckExitsLevelJapanUser(int userID)
        {
            try
            {
                // Create câu truy vấn.
                string query = "USE manageruser; SELECT * from tbl_detail_user_japan WHERE user_id =@userID";
                OpenConnection();
                MySqlCommand comd = new MySqlCommand(query, conn);
                comd.Prepare();
                comd.Parameters.Clear();
                comd.Parameters.AddWithValue("@userID", userID);
                comd.Connection = conn;
                MySqlDataReader sqlDr = comd.ExecuteReader();
                // Trả lại kết quả check trinh do tieng nhat
                return sqlDr.Read();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
            finally
            {
                // Đóng kết nối.
                CloseConnection();
            }
        }
        /// <summary>
        /// insert vào bảng tblDetailUserJapan
        /// Create by NgaPLT
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="conn"></param>
        /// <param name="transaction"></param>
        /// <returns>insert được trả về true, ngược lại trả về false</returns>
            public Boolean InsertDetailUserJapan(UserInfo userInfo, MySqlConnection conn, MySqlTransaction transaction)
        {
            Boolean result = false;
            try
            {
                if (conn != null)
                {
                    String query = "USE manageruser;INSERT INTO tbl_detail_user_japan (user_id, code_level, start_date, end_date, total) VALUES (@userId, @codeLevel, @startDate, @endDate, @total )";

                    MySqlCommand comd = new MySqlCommand(query, conn, transaction);
                    comd.Prepare();
                    comd.Parameters.AddWithValue("@userId", userInfo.UserId);
                    comd.Parameters.AddWithValue("@codeLevel", userInfo.CodeLevel);
                    comd.Parameters.AddWithValue("@startDate", userInfo.StartDate);
                    comd.Parameters.AddWithValue("@endDate", userInfo.EndDate);
                    comd.Parameters.AddWithValue("@total", userInfo.Total);
                    if (comd.ExecuteNonQuery() != -1)
                    {
                        result = true;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }

            // Trả lại kết quả update
            return result;
        }
        /// <summary>
        /// update vào bảng tblDetailUserJapan
        /// Create by NgaPLT
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="conn"></param>
        /// <param name="transaction"></param>
        /// <returns>update được trả về true, ngược lại trả về false</returns>
        public Boolean UpdateTblDetailUserJapan(UserInfo userInfo, MySqlConnection conn, MySqlTransaction transaction)
        {
            Boolean result = false;
            try
            {
                if (conn != null)
                {
                    StringBuilder query = new StringBuilder();
                    query.Append("use manageruser;");
                    query.Append("UPDATE tbl_detail_user_japan");
                    query.Append(" SET code_level = @codeLevel, start_date = @startDate, end_date = @endDate, total = @total");
                    query.Append(" WHERE user_id = @userId");

                    MySqlCommand comd = new MySqlCommand(query.ToString(), conn, transaction);
                    comd.Prepare();
                    comd.Parameters.Clear();
                    comd.Parameters.AddWithValue("@codeLevel", userInfo.CodeLevel);
                    comd.Parameters.AddWithValue("@startDate", userInfo.StartDate);
                    comd.Parameters.AddWithValue("@endDate", userInfo.EndDate);
                    comd.Parameters.AddWithValue("@total", userInfo.Total);
                    comd.Parameters.AddWithValue("@userId", userInfo.UserId);
                    comd.Connection = conn;
                    if (comd.ExecuteNonQuery() != -1)
                    {
                        result = true;
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }

            // Trả lại kết quả update
            return result;
        }
        /// <summary>
        /// delete vào bảng tblDetailUserJapan
        /// Create by NgaPLT
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="conn"></param>
        /// <param name="transaction"></param>
        /// <returns>delete được trả về true, ngược lại trả về false</returns>
        public Boolean DeleteTblDetailUserJapan(int userID, MySqlConnection conn, MySqlTransaction transaction)
        {
            Boolean result = false;
            try
            {
                string query = "USE manageruser; DELETE from tbl_detail_user_japan WHERE user_id =@userID";

                MySqlCommand comd = new MySqlCommand(query, conn, transaction);
                comd.Prepare();
                comd.Parameters.Clear();
                comd.Parameters.AddWithValue("@userID", userID);
                comd.Connection = conn;
                if (comd.ExecuteNonQuery() != -1)
                {
                    result = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }

            // Trả lại kết quả update
            return result;
        }
    }
}