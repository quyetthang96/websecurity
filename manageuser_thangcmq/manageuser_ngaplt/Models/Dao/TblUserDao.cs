﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using manageruser.Models.Utils;
using manageruser.Models.Entity;
using System.Text;

namespace manageruser.Models.Dao
{
    /// <summary>
    /// Class các thao tác tới bảng tbl_user
    /// Create by NgaPLT
    /// </summary>
    public class TblUserDao : BaseDao
    {
        protected MySqlDataReader sqlData;
        /// <summary>
        /// Kiểm tra xem acc login có đúng không
        /// Created by Nga PLT
        /// </summary>
        /// <param name="loginName"></param>
        /// <param name="password"></param>
        /// <returns>nếu acc đc phép login trả về true, ngược lại trả về false</returns>
        public TblUser CheckLogin(string loginName)
        {
            TblUser tblUser = null;
            try
            {
                OpenConnection();
                string query = "select * from tbl_user where login_name = @loginName and rule = 1;";
                MySqlCommand comd = new MySqlCommand(query, conn);
                comd.Prepare();
                comd.Parameters.Clear();
                comd.Parameters.AddWithValue("@loginName", Common.ReplaceWildCard(loginName));
                comd.Connection = conn;
                sqlData = comd.ExecuteReader();
                while(sqlData.Read())
                {
                    tblUser = new TblUser();
                    tblUser.LoginName = sqlData["login_name"].ToString();
                    tblUser.Password = sqlData["password"].ToString();
                    tblUser.Salt = sqlData["saft"].ToString();
                }
            }
            catch (MySqlException e)
            {
                throw e;
            }
            finally
            {
                // ĐÓng kết nối với DB.
                CloseConnection();
            }
            return tblUser;
        }
        /// <summary>
        ///  lấy tblUser theo loginname
        ///  Create by NgaPLT
        /// </summary>
        /// <param name="loginName"></param>
        /// <returns> tblUser</returns>
        public TblUser GetUserByLoginName(string loginName)
        {
            TblUser tblUser = null;
            try
            {
                OpenConnection();
                string query = "use manageruser; select * from tbl_user where login_name = @loginName ;";
                MySqlCommand comd = new MySqlCommand(query, conn);
                comd.Prepare();
                comd.Parameters.Clear();
                comd.Parameters.AddWithValue("@loginName", Common.ReplaceWildCard(loginName));
                comd.Connection = conn;
                sqlData = comd.ExecuteReader();
                while (sqlData.Read())
                {
                    tblUser = new TblUser();
                    tblUser.LoginName = sqlData["login_name"].ToString();
                }
            }
            catch (MySqlException e)
            {
                throw e;
            }
            finally
            {
                // ĐÓng kết nối với DB.
                CloseConnection();
            }
            return tblUser;
        }
        public UserInfo GetUserBySaft(string saft)
        {
             UserInfo userInfo = null;
            try
            {
                OpenConnection();
                StringBuilder query = new StringBuilder();
                query.Append(" select * ");
                query.Append(" from tbl_user u left join mst_group g on u.group_id = g.group_id ");
                query.Append(" left join tbl_detail_user_japan duj on u.user_id = duj.user_id ");
                query.Append(" left join mst_japan j on duj.code_level = j.code_level where u.rule = 0  ");
                query.Append(" and u.saft = @userId ");
                MySqlCommand comd = new MySqlCommand(query.ToString(), conn);
                comd.Prepare();
                comd.Parameters.Clear();
                comd.Parameters.AddWithValue("@userId", saft);
                comd.Connection = conn;
                sqlData = comd.ExecuteReader();
                while (sqlData.Read())
                {
                    userInfo = new UserInfo();
                    userInfo.LoginName = sqlData["login_name"].ToString();
                    userInfo.UserId = (int)sqlData["user_id"];
                    userInfo.FullName = sqlData["full_name"].ToString();
                    userInfo.FullNameKana = sqlData["full_name_kana"].ToString();
                    userInfo.Birthday = (DateTime)sqlData["birthday"];
                    userInfo.GroupId = (int)sqlData["group_id"];
                    userInfo.GroupName = sqlData["group_name"].ToString();
                    userInfo.Email = sqlData["email"].ToString();
                    userInfo.Tel = sqlData["tel"].ToString();
                    userInfo.NameCodeLevel = sqlData["name_level"].ToString();
                    if (userInfo.NameCodeLevel != "")
                    {
                        userInfo.CodeLevel = sqlData["code_level"].ToString();
                        userInfo.StartDate = (DateTime)sqlData["start_date"];
                        userInfo.EndDate = (DateTime)sqlData["end_date"];
                        userInfo.Total = sqlData["total"].ToString();
                    }
                }
            }
            catch (MySqlException e)
            {
                throw e;
            }
            finally
            {
                // ĐÓng kết nối với DB.
                CloseConnection();
            }
            return userInfo;
        }
        public UserInfo GetUserByUserId(int userId)
        {
            UserInfo userInfo = null;
            try
            {
                OpenConnection();
                StringBuilder query = new StringBuilder();
                query.Append(" select * ");
                query.Append(" from tbl_user u left join mst_group g on u.group_id = g.group_id ");
                query.Append(" left join tbl_detail_user_japan duj on u.user_id = duj.user_id ");
                query.Append(" left join mst_japan j on duj.code_level = j.code_level where u.rule = 0  ");
                query.Append(" and u.user_id = @userId ");
                MySqlCommand comd = new MySqlCommand(query.ToString(), conn);
                comd.Prepare();
                comd.Parameters.Clear();
                comd.Parameters.AddWithValue("@userId", userId);
                comd.Connection = conn;
                sqlData = comd.ExecuteReader();
                while (sqlData.Read())
                {
                    userInfo = new UserInfo();
                    userInfo.LoginName = sqlData["login_name"].ToString();
                    userInfo.UserId = (int)sqlData["user_id"];
                    userInfo.FullName = sqlData["full_name"].ToString();
                    userInfo.FullNameKana = sqlData["full_name_kana"].ToString();
                    userInfo.Birthday = (DateTime)sqlData["birthday"];
                    userInfo.GroupId = (int)sqlData["group_id"];
                    userInfo.GroupName = sqlData["group_name"].ToString();
                    userInfo.Email = sqlData["email"].ToString();
                    userInfo.Tel = sqlData["tel"].ToString();
                    userInfo.NameCodeLevel = sqlData["name_level"].ToString();
                    if (userInfo.NameCodeLevel != "")
                    {
                        userInfo.CodeLevel = sqlData["code_level"].ToString();
                        userInfo.StartDate = (DateTime)sqlData["start_date"];
                        userInfo.EndDate = (DateTime)sqlData["end_date"];
                        userInfo.Total = sqlData["total"].ToString();
                    }
                }
            }
            catch (MySqlException e)
            {
                throw e;
            }
            finally
            {
                // ĐÓng kết nối với DB.
                CloseConnection();
            }
            return userInfo;
        }
        /// <summary>
        ///  lấy user theo email
        ///  Create by NgaPLT
        /// </summary>
        /// <param name="email"></param>
        /// <returns>tblUser</returns>
        public TblUser GetUserByEmail(string email)
        {
            TblUser tblUser = null;
            try
            {
                OpenConnection();
                string query = " select * from tbl_user where email = @email ;";
                MySqlCommand comd = new MySqlCommand(query, conn);
                comd.Prepare();
                comd.Parameters.Clear();
                comd.Parameters.AddWithValue("@email", email);
                comd.Connection = conn;
                sqlData = comd.ExecuteReader();
                while (sqlData.Read())
                {
                    tblUser = new TblUser();
                    tblUser.UserId = (int)sqlData["user_id"];
                }
            }
            catch (MySqlException e)
            {
                throw e;
            }
            finally
            {
                // ĐÓng kết nối với DB.
                CloseConnection();
            }
            return tblUser;
        }
        /// <summary>
        /// Lấy listUser theo yêu cầu
        /// Create by NgaPLT
        /// </summary>
        /// <param name="offSet"></param>
        /// <param name="limit"></param>
        /// <param name="groupId"></param>
        /// <param name="fullName"></param>
        /// <param name="sortType"></param>
        /// <param name="sortByFullName"></param>
        /// <param name="sortByCodeLevel"></param>
        /// <param name="sortByEndDate"></param>
        /// <returns></returns>
        public List<UserInfo> GetListUser(int offSet, int limit, int groupId, string fullName, string sortType, string sortByFullName, string sortByCodeLevel, string sortByEndDate )
        {
            // Khởi tạo mảng lưu list userInfo
            List<UserInfo> listUserInfo = new List<UserInfo>();
            try
            {
                OpenConnection();
                StringBuilder query = new StringBuilder();
                query.Append(" select u.user_id, u.full_name, u.birthday, u.email, g.group_name, u.tel, j.name_level, duj.end_date, duj.total, u.saft ");
                query.Append(" from tbl_user u left join mst_group g on u.group_id = g.group_id ");
                query.Append(" left join tbl_detail_user_japan duj on u.user_id = duj.user_id ");
                query.Append(" left join mst_japan j on duj.code_level = j.code_level where u.rule = 0  ");
                if(groupId != 0)
                {
                    query.Append(" and u.group_id = @groupId ");
                }
                if(fullName != null)
                {
                    query.Append(" and u.full_name like @fullName ");
                }
                query.Append(" order by");

                if ("full_name" == sortType)
                {
                    query.Append(" u.full_name ");
                    query.Append(sortByFullName);
                    query.Append(", duj.code_level ");
                    query.Append(sortByCodeLevel);
                    query.Append(",duj.end_date ");
                    query.Append(sortByEndDate);
                }
                else if ("end_date" == sortType)
                {
                    query.Append(" duj.end_date ");
                    query.Append(sortByEndDate);
                    query.Append(", u.full_name ");
                    query.Append(sortByFullName);
                    query.Append(", duj.code_level ");
                    query.Append(sortByCodeLevel);
                }
                else
                {
                    query.Append(" duj.code_level ");
                    query.Append(sortByCodeLevel);
                    query.Append(", u.full_name ");
                    query.Append(sortByFullName);
                    query.Append(", duj.end_date ");
                    query.Append(sortByEndDate);
                }
                query.Append(" limit " + offSet + ", " + limit);
                MySqlCommand comd = new MySqlCommand(query.ToString(), conn);
                comd.Prepare();
                comd.Parameters.Clear();
                comd.Parameters.AddWithValue("@groupId", groupId);
                comd.Parameters.AddWithValue("@fullName", "%" + Common.ReplaceWildCard(fullName) + "%");
               
                comd.Connection = conn;
                sqlData = comd.ExecuteReader();
                while (sqlData.Read())
                {
                    UserInfo userInfo = new UserInfo();
                    userInfo.UserId = (int)sqlData["user_id"];
                    userInfo.FullName = sqlData["full_name"].ToString();
                    userInfo.Birthday = (DateTime)sqlData["birthday"];
                    userInfo.GroupName = sqlData["group_name"].ToString();
                    userInfo.Email = sqlData["email"].ToString();
                    userInfo.Tel = sqlData["tel"].ToString();
                    userInfo.NameCodeLevel = sqlData["name_level"].ToString();
                    userInfo.Saft = sqlData["saft"].ToString();
                    if(userInfo.NameCodeLevel != "")
                    {
                        userInfo.EndDate = (DateTime)sqlData["end_date"];
                        userInfo.Total = sqlData["total"].ToString();
                    }
                    
                    
                   

                    listUserInfo.Add(userInfo);
                }
            }
            catch (MySqlException e)
            {
                throw e;
            }
            finally
            {
                // ĐÓng kết nối với DB.
                CloseConnection();
            }
            return listUserInfo;
        }
        /// <summary>
        /// Lấy số lượng user thỏa mãn yêu cầu
        /// Create by NgaPLT
        /// </summary>
        /// <param name="fullName"></param>
        /// <param name="groupId"></param>
        /// <returns> số lượng user</returns>
        public int GetTotalUser(string fullName, int groupId)
        {
            int total = 0;
            try
            {
                OpenConnection();
                StringBuilder query = new StringBuilder();
                query.Append(" select COUNT(*) as totalUser ");
                query.Append(" from tbl_user u left join mst_group g on u.group_id = g.group_id where u.rule = 0  ");
                if (groupId != 0)
                {
                    query.Append(" and u.group_id = @groupId ");
                }
                if (fullName != null)
                {
                    query.Append(" and u.full_name like @fullName ");
                }
                MySqlCommand comd = new MySqlCommand(query.ToString(), conn);
                comd.Prepare();
                comd.Parameters.Clear();
                comd.Parameters.AddWithValue("@groupId", groupId);
                comd.Parameters.AddWithValue("@fullName", "%" + Common.ReplaceWildCard(fullName) + "%");
                comd.Connection = conn;
                sqlData = comd.ExecuteReader();
                while (sqlData.Read())
                {
                    total = int.Parse(sqlData["totalUser"].ToString());
                }
            }
            catch (MySqlException e)
            {
                throw e;
            }
            finally
            {
                // ĐÓng kết nối với DB.
                CloseConnection();
            }
            return total;
        }
        /// <summary>
        /// insert vào tbl_user
        /// Create by NgaPLT
        /// </summary>
        /// <param name="tblUser"></param>
        /// <param name="conn"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public int InsertTblUser(UserInfo tblUser, MySqlConnection conn, MySqlTransaction transaction)
        {
           int result = -1;
            try
            {
                String query = "INSERT INTO tbl_user(group_id, login_name, password, full_name, full_name_kana, email, tel, birthday, rule, saft) VALUES (@group_id,@loginName,@passwords,@full_name,@full_name_kana,@email,@tel,@birthday, 0, @saft)";

                MySqlCommand comd = new MySqlCommand(query, conn, transaction);
                comd.Prepare();
                comd.Parameters.AddWithValue("@group_id", tblUser.GroupId);
                comd.Parameters.AddWithValue("@loginName", tblUser.LoginName);
                comd.Parameters.AddWithValue("@passwords", tblUser.Password);
                comd.Parameters.AddWithValue("@full_name", tblUser.FullName);
                comd.Parameters.AddWithValue("@full_name_kana", tblUser.FullNameKana);
                comd.Parameters.AddWithValue("@email", tblUser.Email);
                comd.Parameters.AddWithValue("@tel", tblUser.Tel);
                comd.Parameters.AddWithValue("@birthday", tblUser.Birthday);
                comd.Parameters.AddWithValue("@saft", tblUser.Saft);
                comd.Connection = conn;
                if (comd.ExecuteNonQuery() != -1)
                {
                    result = Convert.ToInt32(comd.LastInsertedId);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
            // Trả lại kết quả update
            return result;
        }
        /// <summary>
        /// update bảng tblUser
        /// Create by NgaPLT
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="conn"></param>
        /// <param name="transaction"></param>
        /// <returns> update thành công trả về true, ngược lại false</returns>
        public bool UpdateTblUser(UserInfo userInfo, MySqlConnection conn, MySqlTransaction transaction)
        {
            bool result = false;
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append("update tbl_user");
                query.Append(
                        " set group_id =@group_id, full_name =@full_name, full_name_kana =@full_name_kana, email =@email, tel =@tel, birthday =@birthday, rule = 0");
                query.Append(" where user_id =@user_id");

                MySqlCommand comd = new MySqlCommand(query.ToString(), conn, transaction);
                comd.Prepare();
                comd.Parameters.Clear();
                comd.Parameters.AddWithValue("@group_id", userInfo.GroupId);
                comd.Parameters.AddWithValue("@full_name", userInfo.FullName);
                comd.Parameters.AddWithValue("@full_name_kana", userInfo.FullNameKana);
                comd.Parameters.AddWithValue("@email", userInfo.Email);
                comd.Parameters.AddWithValue("@tel", userInfo.Tel);
                comd.Parameters.AddWithValue("@birthday", userInfo.Birthday);
                comd.Parameters.AddWithValue("@user_id", userInfo.UserId);
                comd.Connection = conn;
                if (comd.ExecuteNonQuery() != -1)
                {
                    result = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }

            // Trả lại kết quả update
            return result;
        }
        /// <summary>
        /// delete tblUser
        /// Create by NgaPLT
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="conn"></param>
        /// <param name="transaction"></param>
        /// <returns> delete thành công trả về true, ngược lại false</returns>
        public Boolean DeleteTblUser(int userID, MySqlConnection conn, MySqlTransaction transaction)
        {
            Boolean result = false;
            try
            {
                string query = " DELETE from tbl_user WHERE user_id =@userID AND rule = 0";


                MySqlCommand comd = new MySqlCommand(query, conn, transaction);
                comd.Prepare();
                comd.Parameters.Clear();
                comd.Parameters.AddWithValue("@userID", userID);
                comd.Connection = conn;
                if (comd.ExecuteNonQuery() != -1)
                {
                    result = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }

            // Trả lại kết quả update
            return result;
        }

    }
}