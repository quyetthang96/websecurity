﻿using System;
using System.Collections.Generic;
using manageruser.Models.Entity;
using MySql.Data.MySqlClient;

namespace manageruser.Models.Dao
{
    /// <summary>
    /// Class thao tác tới bảng mstGroup
    /// Create by NgaPLT
    /// </summary>
    public class MstGroupDao : BaseDao
    {
        protected MySqlDataReader sqlData;
        /// <summary>
        /// Lấy list group
        /// Create by NgaPLT
        /// </summary>
        /// <returns>List<MstGroup></returns>
        public List<MstGroup> GetListMstGroup()
        {
            List<MstGroup> listGroup = new List<MstGroup>();
            try
            {
                OpenConnection();
                string query = "use manageruser; select * from mst_group  order by group_id";
                MySqlCommand sqlCom = new MySqlCommand(query, conn);
                sqlCom.Prepare();
                sqlCom.Connection = conn;
                sqlData = sqlCom.ExecuteReader();
                while (sqlData.Read())
                {
                    MstGroup mstGroup = new MstGroup();
                    mstGroup.GroupId = (int)sqlData["group_id"];
                    mstGroup.GroupName = sqlData["group_name"].ToString();
                    listGroup.Add(mstGroup);

                }

            }
            catch (MySqlException e)
            {
                Console.WriteLine(e);
                throw e;
            }
            finally
            {
                // ĐÓng kết nối với DB.
                CloseConnection();
            }
            return listGroup;
        }
        /// <summary>
        /// lấy group theo id
        /// Create by NgaPLT
        /// </summary>
        /// <param name="id"></param>
        /// <returns>MstGroup</returns>
        public MstGroup GetGroupById(int id)
        {
            MstGroup mstGroup = null;
            try
            {
                OpenConnection();
                string query = "use manageruser; select * from mst_group where group_id = @id ;";
                MySqlCommand comd = new MySqlCommand(query, conn);
                comd.Prepare();
                comd.Parameters.Clear();
                comd.Parameters.AddWithValue("@id", id);
                comd.Connection = conn;
                sqlData = comd.ExecuteReader();
                while (sqlData.Read())
                {
                    mstGroup = new MstGroup();
                    mstGroup.GroupName = sqlData["group_name"].ToString();
                }
            }
            catch (MySqlException e)
            {
                throw e;
            }
            finally
            {
                // ĐÓng kết nối với DB.
                CloseConnection();
            }
            return mstGroup;
        }
    }
}