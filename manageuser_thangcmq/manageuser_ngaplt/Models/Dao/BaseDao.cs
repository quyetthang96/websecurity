﻿using System;
using MySql.Data.MySqlClient;
using manageruser.Models.Utils;


namespace manageruser.Models.Dao
{
    /// <summary>
    /// Class mở đóng kết nối tới DB
    /// Create by NgaPLT
    /// </summary>
    public class BaseDao
    {
        // Khai báo con
        private string conString = ConstansFormat.CONECT;
        public MySqlConnection conn { get; set; }
        /// <summary>
        /// Mở kết nối connection
        /// Create by NgaPLT
        /// </summary>
        public void OpenConnection()
        {
            try
            {
                // Mở kết nối 
                conn = new MySqlConnection(conString);
                conn.Open();
            }
            catch (Exception e)
            {
                // In lỗi nếu có.
                Console.WriteLine(e);
                throw e;
            }

        }
        /// <summary>
        /// Đóng kết nối connection
        /// Create by NgaPLT
        /// </summary>
        public void CloseConnection()
        {
            try
            {
                // check conn khác null thì mới close.
                if (conn != null)
                {
                    // Đóng kết nối.
                    conn.Close();
                }
            }
            catch (MySqlException e)
            {
                // In ra lỗi.
                Console.WriteLine(e);
                throw e;
            }

        }
    }
}