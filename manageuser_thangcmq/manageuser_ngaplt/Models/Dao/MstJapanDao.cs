﻿using manageruser.Models.Entity;
using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace manageruser.Models.Dao
{
    /// <summary>
    /// Class thao tác tới bảng mstJapan
    /// Create by NgaPLT
    /// </summary>
    public class MstJapanDao : BaseDao
    {
        protected MySqlDataReader sqlData;
        /// <summary>
        /// lấy list japan
        /// Create by NgaPLT
        /// </summary>
        /// <returns> List<MstJapan></returns>
        public List<MstJapan> GetListMstJapan()
        {
            // Khởi tạo một ArrayList lưu listMstGroup.
            List<MstJapan> listMstJapan = new List<MstJapan>();
            try
            {
                OpenConnection();
                String query = "use manageruser; select * from mst_japan order by code_level;";
                MySqlCommand comd = new MySqlCommand(query, conn);
                comd.Connection = conn;
                sqlData = comd.ExecuteReader();

              
                while (sqlData.Read())
                {
                  
                    MstJapan mstJapan = new MstJapan();
                    mstJapan.CodeLevel = sqlData["code_level"].ToString();
                    mstJapan.NameLevel = sqlData["name_level"].ToString();
                  
                    listMstJapan.Add(mstJapan);
                }

            }
            catch (MySqlException e)
            {
                Console.WriteLine(e);
                throw e;
            }
            finally
            {
                // try catch nếu khi đóng kết nối có lỗi.
                CloseConnection();
            }
            // Trả lại listMstGroup lấy được
            return listMstJapan;
        }
        /// <summary>
        /// lấy trình độ tiếng nhật theo codeLevel
        /// Create by NgaPLT
        /// </summary>
        /// <param name="codeLevel"></param>
        /// <returns>mstJapan</returns>
        public MstJapan GetJapanById(string codeLevel)
        {
            MstJapan mstJapan = null;
            try
            {
                OpenConnection();
                string query = "use manageruser; select * from mst_japan where code_level = @id ;";
                MySqlCommand comd = new MySqlCommand(query, conn);
                comd.Prepare();
                comd.Parameters.Clear();
                comd.Parameters.AddWithValue("@id", codeLevel);
                comd.Connection = conn;
                sqlData = comd.ExecuteReader();
                while (sqlData.Read())
                {
                    mstJapan = new MstJapan();
                    mstJapan.CodeLevel = sqlData["code_level"].ToString();
                }
            }
            catch (MySqlException e)
            {
                throw e;
            }
            finally
            {
                // ĐÓng kết nối với DB.
                CloseConnection();
            }
            return mstJapan;
        }
    }

}