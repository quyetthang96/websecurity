﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace manageruser.Models.Entity
{
    /// <summary>
    /// Class đối tượng userInfo
    /// Create by NgaPLT 18/12/2019
    /// </summary>
    public class UserInfo
    {
        public string Saft { get; set; }
        public int Rule { get; set; }
        public int UserId { get; set; }
        public String LoginName { get; set; }
        public String FullName { get; set; }
        public String FullNameKana { get; set; }
        public DateTime Birthday { get; set; }
        public String GroupName { get; set; }
        public int GroupId { get; set; }
        public String Email { get; set; }
        public String Tel { get; set; }
        public String CodeLevel { get; set; }
        public String NameCodeLevel { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public String Total { get; set; }
        public String Password { get; set; }
        public String PasswordConfirm { get; set; }
        public int BirthYear { get; set; }
        public int BirthMonth { get; set; }
        public int BirthDay { get; set; }
        public int StartYear { get; set; }
        public int StartMonth { get; set; }
        public int StartDay { get; set; }
        public int EndYear { get; set; }
        public int EndMonth { get; set; }
        public int EndDay { get; set; }

    }
}