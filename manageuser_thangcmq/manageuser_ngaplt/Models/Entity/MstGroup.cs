﻿using System;

namespace manageruser.Models.Entity
{
    /// <summary>
    /// Class đối tượng mstGroup
    /// Create by NgaPLT 18/12/2019
    /// </summary>
    public class MstGroup
    {
        public int GroupId { get; set; }
        public String GroupName { get; set; }
    }
}