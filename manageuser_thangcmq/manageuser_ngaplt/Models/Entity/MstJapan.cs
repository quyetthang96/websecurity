﻿using System;

namespace manageruser.Models.Entity
{
    /// <summary>
    /// Class đối tượng mstJapan
    /// Create by NgaPLT 18/12/2019
    /// </summary>
    public class MstJapan
    {
        public String CodeLevel { get; set; }
        public String NameLevel { get; set; }
    }
}