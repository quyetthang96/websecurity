﻿using System;

namespace manageruser.Models.Entity
{
    /// <summary>
    /// Class đối tượng tblUser
    /// Create by NgaPLT 18/12/2019
    /// </summary>
    public class TblUser
    {
        // Các thuộc tính của bảng
        public int UserId { get; set; }
        public int GroupId { get; set; }
        public String LoginName { get; set; }
        public String Password { get; set; }
        public String FullName { get; set; }
        public String FullNameKana { get; set; }
        public String Email { get; set; }
        public String Tel { get; set; }
        public DateTime Birthday { get; set; }
        public String Salt { get; set; }
        public int Category { get; set; }
    }
}