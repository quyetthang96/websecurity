﻿using System;
using System.Collections.Generic;
using manageruser.Models.Dao;
using manageruser.Models.Entity;

namespace manageruser.Models.Logic
{
    /// <summary>
    /// Class các thao tác logic trên mst_japan
    /// Create by NgaPLT
    /// </summary>
    public class MstJapanLogic
    {
        private MstJapanDao mstJapanDao = new MstJapanDao();
        /// <summary>
        /// lấy list mst japan
        /// Create by NgaPLT
        /// </summary>
        /// <returns>list<mstJapan></mstJapan></returns>
        public List<MstJapan> GetListMstJapan()
        {
            try
            {
                return mstJapanDao.GetListMstJapan();
            }catch(Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// check trình độ tiếng nhật theo id
        /// Create by NgaPLT
        /// </summary>
        /// <param name="codeLevel"></param>
        /// <returns></returns>
        public Boolean CheckJapanById(string codeLevel)
        {
            try
            {
                if (mstJapanDao.GetJapanById(codeLevel) != null)
                {
                    return true;
                }
                return false;
            }catch(Exception e)
            {
                throw e;
            }
        }
    }
}