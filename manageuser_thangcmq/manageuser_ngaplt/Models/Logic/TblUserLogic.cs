﻿using System;
using System.Collections.Generic;
using manageruser.Models.Dao;
using manageruser.Models.Entity;
using manageruser.Models.Utils;

namespace manageruser.Models.Logic
{
    /// <summary>
    /// Class các thao tác logic trên tbl_user
    /// Create by NgaPLT
    /// </summary>
    public class TblUserLogic
    {
        private TblUserDao tblUserDao = new TblUserDao();
        /// <summary>
        /// Kiểm tra login
        /// Create by NgaPLT
        /// </summary>
        /// <param name="loginName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Boolean CheckLogin(string loginName, string password)
        {
            bool resul = false;
            try
            {
                Common common = new Common();
                TblUser tblUser = tblUserDao.CheckLogin(loginName);
                if (tblUser != null)
                {
                    if (tblUser.Password.Equals(common.GetMD5(password + tblUser.Salt)))
                    {
                        resul = true;
                    }
                    
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return resul;
        }
        /// <summary>
        /// Lấy ra list user theo yêu cầu
        /// </summary>
        /// <param name="offSet"></param>
        /// <param name="limit"></param>
        /// <param name="groupId"></param>
        /// <param name="fullName"></param>
        /// <param name="sortType"></param>
        /// <param name="sortByFullName"></param>
        /// <param name="sortByCodeLevel"></param>
        /// <param name="sortByEndDate"></param>
        /// <returns> list user</returns>
        public List<UserInfo> GetListUser(int offSet, int limit, int groupId, string fullName, string sortType, string sortByFullName, string sortByCodeLevel, string sortByEndDate)
        {
            try
            {
            return tblUserDao.GetListUser(offSet, limit, groupId, fullName, sortType, sortByFullName, sortByCodeLevel, sortByEndDate);

            }catch(Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Lấy số lượng user phù hợp yêu cầu 
        /// Create by NgaPLT
        /// </summary>
        /// <param name="fullName"></param>
        /// <param name="groupId"></param>
        /// <returns>số lượng user phù hợp yêu cầu </returns>
        public int GetTotalUser(string fullName, int groupId)
        {
            try
            {
                return tblUserDao.GetTotalUser(fullName, groupId);
            }catch(Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Check login
        /// Create by NgaPLT
        /// </summary>
        /// <param name="loginName"></param>
        /// <returns> nếu được login trả về true, ngược lại trả về false</returns>
        public Boolean CheckExitLogin(string loginName)
        {
            try
            {
                if (tblUserDao.GetUserByLoginName(loginName) != null)
                {
                    return true;
                }
                return false;
            }catch(Exception e)
            {
                throw e;
            }
        }
        public UserInfo GetUserBySaft(string saft)
        {
            try
            {
                return tblUserDao.GetUserBySaft(saft);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Lấy ra userInfo theo id
        /// Create by NgaPLT
        /// </summary>
        /// <param name="userId"></param>
        /// <returns> userInfo</returns>
        public UserInfo GetUserByUserId(int userId)
        {
            try
            {
                return tblUserDao.GetUserByUserId(userId);
            }catch(Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Kiểm tra email
        /// Create by NgaPLT
        /// </summary>
        /// <param name="email"></param>
        /// <param name="userId"></param>
        /// <returns> nếu email đã tồn tại trả về true, ngược lại trả về false </returns>
        public Boolean CheckExitEmail(string email, int userId)
        {
            try
            {
                TblUser tblUser = tblUserDao.GetUserByEmail(email);
                if (tblUser != null && tblUser.UserId != userId)
                {
                    return true;
                }
                return false;
            }catch(Exception e)
            {
                throw e;
            }
        }


    }
}