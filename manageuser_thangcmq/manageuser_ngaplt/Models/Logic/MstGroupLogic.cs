﻿using System;
using System.Collections.Generic;
using manageruser.Models.Dao;
using manageruser.Models.Entity;

namespace manageruser.Models.Logic
{
    /// <summary>
    /// Class các hành động đến Mst_group
    /// Create by NgaPLT
    /// </summary>
    public class MstGroupLogic
    {
        private MstGroupDao mstGroupDao = new MstGroupDao();
        /// <summary>
        /// Lấy ra list group
        /// Create by NgaPLT
        /// </summary>
        /// <returns> list group </returns>
        public List<MstGroup> GetListMstGroup()
        {
            try
            {
            return mstGroupDao.GetListMstGroup();
            }catch(Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Kiểm tra groupId có tồn tại hay không
        /// Create by NgaPLT
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Nếu tồn tại trả về true, ngược lại trả về false</returns>
        public Boolean CheckGroupById(int id)
        {
            try
            {
                if (mstGroupDao.GetGroupById(id) != null)
                {
                    return true;
                }
                return false;
            }catch(Exception e)
            {
                throw e;
            }
        }
    }
}