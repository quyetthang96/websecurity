﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;

namespace manageruser.Models.Utils
{
    /// <summary>
    /// Class các phương thưc static
    /// Create by NgaPLT
    /// </summary>
    public class Common
    {
        public String randomString()
        {
            Random rnd = new Random();
            int number = rnd.Next(10000, 99999);
            string text = GetMD5(number.ToString());
            text = text.ToUpper();
            text = text.Substring(0, 6);
            return text;

        }
        // Xử lý MD5
        public String GetMD5(string txt)
        {
            String str = "";
            Byte[] buffer = System.Text.Encoding.UTF8.GetBytes(txt);
            System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            buffer = md5.ComputeHash(buffer);
            foreach (Byte b in buffer)
            {
                str += b.ToString("X2");
            }
            return str;
        }
        /// <summary>
        /// replaceWildCard 
        /// Create by NgaPLT
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static String ReplaceWildCard(String value)
        {
            if (value != null)
            {
                value = value.Trim();
                value = value.Replace("\\", "\\\\");
                value = value.Replace("%", "\\%");
                value = value.Replace("_", "\\_");
            }
            return value;
        }
        /// <summary>
        /// getOffSet
        /// Create by NgaPLT
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="currentPage"></param>
        /// <returns></returns>
        public static int GetOffSet(int limit, int currentPage)
        {
            return limit * (currentPage - 1);
        }
        /// <summary>
        /// Lấy số lượng page
        /// Create by NgaPLT
        /// </summary>
        /// <param name="totalUser"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public static int GetTotalPage(int totalUser, int limit)
        {
            int totalPage = 0;
                // nếu totalUser / limit chẵn thì số page = kết quả chia
                if ((totalUser % limit) == 0)
                {
                    totalPage = totalUser / limit;
                }
                else
                { // nếu totalUser / limit lẻ thì số page = kết quả chia + 1
                    totalPage = totalUser / limit + 1;
                }
            return totalPage;
        }
        /// <summary>
        /// lấy list paging
        /// Create by NGaPLT
        /// </summary>
        /// <param name="totalPage"></param>
        /// <param name="limit"></param>
        /// <param name="currentPage"></param>
        /// <returns></returns>
        public static List<int> GetListPaging(int totalPage, int limit, int currentPage)
        {
            List<int> listPaging = new List<int>();
                 if( totalPage == 1)
            {
                totalPage = 0;
            }
                // Lấy về số thứ tự trang được hiển thị đầu tiền
                int firstPage = (currentPage - 1) / ConstansFormat.LIMIT_PAGE * ConstansFormat.LIMIT_PAGE + 1;
                // Lấy về số thứ tự trang được hiển thị cuối cùng
                int endPage = firstPage + ConstansFormat.LIMIT_PAGE - 1;
                if (endPage > totalPage)
                {
                    endPage = totalPage;
                }
                for (int i = firstPage; i <= endPage; i++)
                {
                    listPaging.Add(i);
                }
            
            return listPaging;
        }
        /// <summary>
        /// Lấy ngày tháng năm hiện tại
        /// Create by NgaPLT
        /// </summary>
        /// <returns></returns>
        public static DateTime GetTimeNow()
        {
            return DateTime.Now;
        }
        /// <summary>
        /// lấy list year
        /// Create by NgaPLT
        /// </summary>
        /// <param name="yearNow"></param>
        /// <returns></returns>
        public static List<int> GetListYear(int yearNow)
        {
            List<int> listYear = new List<int>();
            for(int i = 1960; i <= yearNow; i++)
            {
                listYear.Add(i);
            }
            return listYear;
        }
        /// <summary>
        /// lấy list month
        /// Create by NgaPLT
        /// </summary>
        /// <returns></returns>
        public static List<int> GetListMonth()
        {
            List<int> listMonth = new List<int>();
            for (int i = 1; i <= 12; i++)
            {
                listMonth.Add(i);
            }
            return listMonth;
        }
        /// <summary>
        /// lấy list day
        /// Create by NgaPLT
        /// </summary>
        /// <returns></returns>
        public static List<int> GetListDay()
        {
            List<int> listDay = new List<int>();
            for (int i = 1; i <= 31; i++)
            {
                listDay.Add(i);
            }
            return listDay;
        }
        /// <summary>
        /// check độ dài
        /// Create by NgaPLT
        /// </summary>
        /// <param name="value"></param>
        /// <param name="max"></param>
        /// <param name="min"></param>
        /// <returns></returns>
        public static bool CheckLength(string value, int max, int min)
        {
            bool check = true;
            if(value.Length < min || value.Length > max)
            {
                check = false;
            }
            return check;
        }
        /// <summary>
        /// check format
        /// Create by NgaPLT
        /// </summary>
        /// <param name="str"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static Boolean CheckFormat(string str, string format)
        {
            return Regex.IsMatch(str, format);

        }
        /// <summary>
        /// check ký tự 1 byte
        /// Create by NgaPLT 
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static Boolean CheckByte(String password)
        {
            for (int i = 0; i < password.Length; i++)
            {
                char c = password[i];
                if (c < 0 || c > 255)
                {
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// check format date
        /// Create by NgaPLT
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static Boolean CheckFormatDate(int year, int month, int day)
        {
            try
            {
                DateTime value = new DateTime(year, month, day );
            }
            catch
            {
                return false;
            }

            return true;
        }
        /// <summary>
        /// check endDate
        /// Create by NgaPLT
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static Boolean CheckEndDate(int startY, int startM, int startD, int endY, int endM, int endD)
        {
            Boolean ketQua = false;
          
            // Tiến hành kiểm tra lần lượt year->month->day.
            if (startY < endY)
            {
                ketQua = true;
            }
            else if (startY == endY)
            {
                if (startM < endM)
                {
                    ketQua = true;
                }
                else if (startM == endM)
                {
                    if (startD < endD)
                    if (startD < endD)
                    {
                        ketQua = true;
                    }
                }
            }
            return ketQua;
        }
        /// <summary>
        /// add lỗi vào list lỗi
        /// Create by NgaPLT
        /// </summary>
        /// <param name="list"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static List<string> AddErr(List<string> list, string value)
        {
            if(value != null)
            {
                list.Add(value);
            }
            return list;
        }
        /// <summary>
        /// chuyển string sang int
        /// Create by NgaPLT
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ParseInt(string value)
        {
            try
            {
                return int.Parse(value);
            }
            catch( Exception e)
            {
                return 0;
            }
        }
       
    }
}