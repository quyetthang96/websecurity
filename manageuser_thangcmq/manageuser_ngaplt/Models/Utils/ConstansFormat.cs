﻿using System;

namespace manageruser.Models.Utils
{
    /// <summary>
    /// class các format
    /// Create by NgaPLT 21/12/2019
    /// </summary>
    public class ConstansFormat
    {
        public const string CONECT = "server=localhost;user id=root;password=root;persistsecurityinfo=True;database=manageruser";

        public const string ASC = "ASC";
        public const string DESC = "DESC";
        public const int LIMIT = 5;
        public const int LIMIT_PAGE = 3;

        // Format check định dạng trường.
        public const String FORMAT_LOGIN_NAME = "^[a-zA-Z_][a-zA-Z0-9_]+$";
        public const String FORMAT_EMAIL = "[a-zA-Z][\\w]+@[a-zA-Z]+\\.[a-zA-Z]+$";
        public const String FORMAT_TEL = "[0-9]{1,4}-[0-9]{1,4}-[0-9]{1,4}$";
        public const String FORMAT_KANA = "^[ァ-ン]+$";
        public const String FORMAT_TOTAL = "^[0-9]+$";
        // Giới hạn chuỗi ký tự
        public const int MIN_CHAR_LOGIN = 4;
        public const int MIN_CHAR = 5;
        public const int MAX_CHAR = 15;
        public const int MAX_LENGTH = 255;
        public const int MIN_LENGTH = 1;
        public const int MAX_TEL = 14;
        public const int MAX_MAIL = 100;
    }
}