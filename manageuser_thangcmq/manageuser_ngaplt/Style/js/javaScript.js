

// Xử lý ẩn hiện trường trình độ tiếng nhật
function hiddenLevelJapan() {
	var stt = document.getElementById("hiddenLvJapan");
	if (stt.style.display == 'none') {
		stt.style.display = 'block';
	} else {
		stt.style.display = 'none';
	}

}

// xử lí khi click button xóa
function confirmDelete(id) {
	// hiển thị thông báo xác nhận xóa
	var mesage = confirm('削除しますが、よろしいでしょうか。');
	// nếu chọn ok thì redirect sang url
    if (mesage) {
        window.location = "DeleteUser?userId=" + id;
	}
}
