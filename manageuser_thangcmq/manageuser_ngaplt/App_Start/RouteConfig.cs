﻿
using System.Web.Mvc;
using System.Web.Routing;

namespace manageuser_ngaplt
{
    /// <summary>
    /// class RouteConfig
    /// Create by NgaPLT 17/12/2019
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        /// Method RegisterRoutes
        /// Create by NgaPLT 17/12/2019
        /// </summary>
        /// <param name="routes"></param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
               defaults: new { controller = "Login", action = "ADM001", id = UrlParameter.Optional }
            );
        }
    }
}
