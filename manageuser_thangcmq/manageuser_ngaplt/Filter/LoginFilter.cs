﻿
using manageruser.Models.Logic;
using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace manageruser.Filter
{
    /// <summary>
    /// Class filter
    /// Create by NgaPLT 21/12/2019
    /// </summary>
    //[ValidateInput(false)]
    public class LoginFilter : ActionFilterAttribute
    {
        /// <summary>
        /// kiểm tra login
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var loginName = filterContext.HttpContext.Session["LoginName"];
           var pass = filterContext.HttpContext.Session["pass"];
            TblUserLogic tblUserLogic = new TblUserLogic();
            
            if (loginName == null || !tblUserLogic.CheckLogin(loginName.ToString(), pass.ToString()))
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                    { "controller", "Login" },
                    { "action", "ADM001" }
                });
            } 
            base.OnActionExecuting(filterContext);
        }
    }
}