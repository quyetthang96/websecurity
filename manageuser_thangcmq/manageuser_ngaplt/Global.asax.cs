﻿
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace manageuser_ngaplt
{
    /// <summary>
    /// MvcApplication
    /// Create by NgaPLT 17/12/2019
    /// </summary>
    public class MvcApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// run app
        /// Create by NgaPLT 17/12/2019
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            
        }
    }
}
