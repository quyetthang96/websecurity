﻿using manageruser.Filter;
using manageruser.Models.Utils;
using System;
using System.Web.Mvc;

namespace manageruser.Controllers
{
    /// <summary>
    /// Class xử lý hiển thị thông báo
    /// Create by NgaPLT 21/12/2019
    /// </summary>
    public class SusscessController : Controller
    {
        // GET: Susscess
        /// <summary>
        /// hiển thị thông báo
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <returns></returns>
        [LoginFilter]
        public ActionResult ADM006()
        {
            String msg = Request.QueryString["MSG"];
            if(msg == null)
            {
                return RedirectToAction("ADM001", "Login");
            }
            switch (msg)
            {
                case "MSG001":
                    ViewBag.msg = ConstansErr.MSG001;
                    break;
                case "MSG002":
                    ViewBag.msg = ConstansErr.MSG002;
                    break;
                case "MSG003":
                    ViewBag.msg = ConstansErr.MSG003;
                    break;
                default:
                    return RedirectToAction("ADM002", "ListUser");

            }

            return View();
        }
        /// <summary>
        /// Xử lý khi click button ok 
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <returns></returns>
        [LoginFilter]
        public ActionResult List()
        {
            return RedirectToAction("ADM002", "ListUser");
        }
    }
}