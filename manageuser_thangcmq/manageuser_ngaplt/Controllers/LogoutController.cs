﻿using System.Web.Mvc;

namespace manageruser.Controllers
{
    /// <summary>
    /// Class xử lý logout
    /// Create by NgaPLT 18/12/2019
    /// </summary>
    public class LogoutController : Controller
    {
        // GET: Logout
        /// <summary>
        /// Xử lý logout ra khỏi hệ thống
        /// Create by NgaPLT 18/12/2019
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            // Thực hiện xóa Session
            Session.RemoveAll();
            return RedirectToAction("ADM001", "Login");
        }
    }
}