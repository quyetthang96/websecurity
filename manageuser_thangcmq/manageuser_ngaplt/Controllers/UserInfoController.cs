﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using manageruser.Models.Utils;
using manageruser.Models.Logic;
using manageruser.Models.Entity;
using manageruser.Filter;

namespace manageruser.Controllers
{
    /// <summary>
    /// Class hiển thị userInfo
    /// Create by NgaPLT 23/12/2019
    /// </summary>
    public class UserInfoController : Controller
    {
        /// <summary>
        ///  hiển thị userInfo
        /// Create by NgaPLT  23/12/2019
        /// </summary>
        /// <returns></returns>
        // GET: UserInfo
        [LoginFilter]
        public ActionResult ADM005()
        {
            string saft =Request.QueryString["userId"];
            TblUserLogic tblUserLogic = new TblUserLogic();
            UserInfo userInfo = tblUserLogic.GetUserBySaft(saft);
            if (userInfo != null)
            {
                ViewBag.userInfo = userInfo;
            }
            else
            {
                // tra ve man hinh loi
                return RedirectToAction("SystemError", "SystemError", new { MSG = "MSG006" });
            }
            return View();
        }
        /// <summary>
        /// xử lý khi click button edit
        /// Create by NgaPLT 23/12/2019
        /// </summary>
        /// <returns></returns>
        [LoginFilter]
        public ActionResult Edit()
        {
           
            Session["userId"] = Common.ParseInt(Request.QueryString["userId"]);
            // Gọi tới màn hình ADM003 trường hợp edit
            return RedirectToAction("ADM003", "UserInfoInput", new { type = "edit" });
        }
        /// <summary>
        /// Xử lý khi click button delete
        /// Create by NgaPLT 23/12/2019
        /// </summary>
        /// <returns></returns>
        [LoginFilter]
        public ActionResult DeleteUser()
        {
            string id = Request.QueryString["userId"];
            Session["userId"] = Common.ParseInt(Request.QueryString["userId"]);
            // Gọi tới  trường hợp delete
            return RedirectToAction("Delete", "DeleteUserInfo");
        }
        
        }
}