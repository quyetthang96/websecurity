﻿
using manageruser.Filter;
using manageruser.Models.Utils;
using System;
using System.Web.Mvc;

namespace manageruser.Controllers
{
   /// <summary>
   /// Class hiển thị thông báo lỗi
   /// Create by NgaPLT 21/12/2019
   /// </summary>
    public class SystemErrorController : Controller
    {
        // GET: SystemError
        /// <summary>
        /// Hiển thị thông báo lỗi
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <returns></returns>
        [LoginFilter]
        public ActionResult SystemError()
        {
            String msg = Request.QueryString["MSG"];
            switch (msg)
            {
                case "MSG006":
                    ViewBag.msg = ConstansErr.MSG006;
                    break;
                case "MSG005":
                    ViewBag.msg = ConstansErr.MSG005;
                    break;
                default:
                    ViewBag.msg = ConstansErr.MSG006;
                    break;


            }
            return View();
        }
        /// <summary>
        /// xử lý khi click button ok
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <returns></returns>
        [LoginFilter]
        public ActionResult Logout()
        {
            return RedirectToAction("ADM002", "ListUser");
        }
    }
}