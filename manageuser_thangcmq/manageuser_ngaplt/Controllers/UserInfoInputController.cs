﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using manageruser.Filter;
using manageruser.Models.Entity;
using manageruser.Models.Logic;
using manageruser.Models.Utils;
using manageruser.Models.Validates;

namespace manageruser.Controllers
{
    /// <summary>
    /// Class xử lý input add, edit
    /// Create by NgaPLT 21/12/2019
    /// </summary>
    public class UserInfoInputController : Controller
    {
        // GET: AddUserInput
        /// <summary>
        /// Xử lý trường hợp add, edit và submit
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <returns></returns>
        [LoginFilter]
       
        public ActionResult ADM003()
        {
            try
            {
                // lấy action
                string action = Request.QueryString["type"];
                MstGroupLogic mstGroupLogic = new MstGroupLogic();
                MstJapanLogic mstJapanLogic = new MstJapanLogic();
                // lấy list mstgroup
                List<MstGroup> listGroup = new List<MstGroup>();
                listGroup = mstGroupLogic.GetListMstGroup();
                //lấy list mstjapan
                List<MstJapan> listJapan = new List<MstJapan>();
                listJapan = mstJapanLogic.GetListMstJapan();
                // lấy ngày tháng năm hiện tại
                DateTime dateNow = Common.GetTimeNow();
                List<int> listYear = Common.GetListYear(dateNow.Year);
                List<int> listYearEnd = listYear;
                listYearEnd.Add(dateNow.Year + 1);
                List<int> listMonth = Common.GetListMonth();
                List<int> listDay = Common.GetListDay();
                UserInfo userInfo = new UserInfo();
                List<string> listErr = new List<string>();
              
                // nếu là trường hợp add
                 if (action == "add")
                {
                    userInfo.BirthYear = dateNow.Year;
                    userInfo.BirthMonth = dateNow.Month;
                    userInfo.BirthDay = dateNow.Day;
                    userInfo.StartYear = dateNow.Year;
                    userInfo.StartMonth = dateNow.Month;
                    userInfo.StartDay = dateNow.Day;
                    userInfo.EndYear = dateNow.Year + 1;
                    userInfo.EndMonth = dateNow.Month;
                    userInfo.EndDay = dateNow.Day;
                }
                //nếu là trường hớp edit
                else if (action == "edit")
                {
                    TblUserLogic tblUserLogic = new TblUserLogic();
                    int userId = (int)Session["userId"];
                    //kiểm tra userId có tòn tại 
                    userInfo = tblUserLogic.GetUserByUserId(userId);
                    // nếu tồn tại
                    if (userInfo != null)
                    {
                        userInfo.BirthYear = userInfo.Birthday.Year;
                        userInfo.BirthMonth = userInfo.Birthday.Month;
                        userInfo.BirthDay = userInfo.Birthday.Day;
                        // kiểm tra xem có trình độ tiếng nhật
                        if (string.IsNullOrEmpty(userInfo.NameCodeLevel))
                        {
                            userInfo.StartYear = dateNow.Year;
                            userInfo.StartMonth = dateNow.Month;
                            userInfo.StartDay = dateNow.Day;
                            userInfo.EndYear = dateNow.Year + 1;
                            userInfo.EndMonth = dateNow.Month;
                            userInfo.EndDay = dateNow.Day;
                        }
                        else
                        {
                            userInfo.StartYear = userInfo.StartDate.Year;
                            userInfo.StartMonth = userInfo.StartDate.Month;
                            userInfo.StartDay = userInfo.StartDate.Day;
                            userInfo.EndYear = userInfo.EndDate.Year;
                            userInfo.EndMonth = userInfo.EndDate.Month;
                            userInfo.EndDay = userInfo.EndDate.Day;
                        }
                        ViewBag.userInfo = userInfo;
                    }
                    //nếu không tồn tại
                    else
                    {
                        // tra ve man hinh loi
                        return RedirectToAction("SystemError", "SystemError", new { MSG = "MSG005" });
                    }
                }
                // nếu là trường hợp submit
                else if (action == "submit")
                {
                    // lấy giá trị đã nhập
                    userInfo.UserId = (int)Session["userId"];
                    userInfo.LoginName = Request.Unvalidated.Form["loginName"];
                    userInfo.GroupId = int.Parse(Request.Form["groupId"]);
                    userInfo.FullName = Request.Unvalidated.Form["fullName"];
                    userInfo.FullNameKana = Request.Unvalidated.Form["fullNameKana"];
                    userInfo.BirthYear = int.Parse(Request.Form["yearBirthday"]);
                    userInfo.BirthMonth = int.Parse(Request.Form["monthBirthday"]);
                    userInfo.BirthDay = int.Parse(Request.Form["dayBirthday"]);
                   
                    userInfo.Email = Request.Unvalidated.Form["email"];
                    userInfo.Tel = Request.Unvalidated.Form["tel"];
                    if (userInfo.UserId == 0)
                    {
                        userInfo.Password = Request.Unvalidated.Form["password"];
                        userInfo.PasswordConfirm = Request.Unvalidated.Form["passwordConfirm"];

                    }
                    if (!Request.Form["kyu_id"].Equals("0"))
                    {
                        userInfo.CodeLevel = Request.Form["Kyu_id"];
                        userInfo.StartYear = int.Parse(Request.Form["yearStart"]);
                        userInfo.StartMonth = int.Parse(Request.Form["monthStart"]);
                        userInfo.StartDay = int.Parse(Request.Form["dayStart"]);
                        userInfo.EndYear = int.Parse(Request.Form["yearEnd"]);
                        userInfo.EndMonth = int.Parse(Request.Form["monthEnd"]);
                        userInfo.EndDay = int.Parse(Request.Form["dayEnd"]);
                        userInfo.Total = Request.Unvalidated.Form["total"];
                    }
                    else
                    {
                        userInfo.StartYear = dateNow.Year;
                        userInfo.StartMonth = dateNow.Month;
                        userInfo.StartDay = dateNow.Day;
                        userInfo.EndYear = dateNow.Year + 1;
                        userInfo.EndMonth = dateNow.Month;
                        userInfo.EndDay = dateNow.Day;
                    }

                    // kiểm tra các giá trị đã nhập
                    UserValidate userValidate = new UserValidate();
                    listErr = userValidate.ValidateUserInfo(userInfo);
                    //nếu không có lỗi
                    if (listErr.Count == 0)
                    {

                        String key = DateTime.Now.ToString();
                        Session["key"] = key;
                        Session[key] = userInfo;
                        return RedirectToAction("ADM004", "UserInfoConfirm");
                    }

                }
                // trường hợp back
                else if (action == "back")
                {
                    string key = Request.QueryString["key"];
                    userInfo = (UserInfo)Session[key];
                }
                else
                {
                    // tra ve man hinh loi
                    return RedirectToAction("SystemError", "SystemError", new { MSG = "MSG006" });
                }
                Session["userId"] = userInfo.UserId;
                ViewBag.listErr = listErr;

                ViewBag.listJapan = listJapan;
                ViewBag.listGroup = listGroup;
                ViewBag.userInfo = userInfo;
                ViewBag.listYear = listYear;
                ViewBag.listYearEnd = listYearEnd;
                ViewBag.listMonth = listMonth;
                ViewBag.listDay = listDay;
                return View();
            }catch(Exception e)
            {
                // tra ve man hinh loi
                return RedirectToAction("SystemError", "SystemError", new { MSG = "MSG006" });
            }
        }
    }
}