﻿using manageruser.Filter;
using manageruser.Models.Entity;
using manageruser.Models.Logic;
using manageruser.Models.Validates;
using System;
using System.Web.Mvc;

namespace manageruser.Controllers
{
    /// <summary>
    /// class xử lý delete
    /// Create by NgaPLT 23/12/2019
    /// </summary>
    public class DeleteUserInfoController : Controller
    {
        // GET: DeleteUserInfo
        /// <summary>
        /// Xử lý delete userInfo
        /// Create by NgaPLT 23/12/2019
        /// </summary>
        /// <returns></returns>
        [LoginFilter]
        public ActionResult Delete()
        {
            try
            {

            int userId = (int)Session["userId"];
            //kiểm tra userId có tòn tại 
            TblUserLogic tblUserLogic = new TblUserLogic();
            UserInfo userInfo = tblUserLogic.GetUserByUserId(userId);
            if (userInfo != null)
            {
                Boolean result = false;
                UserValidate userValidate = new UserValidate();
                result = userValidate.DeleteserInfo(userId);
                if (result)
                {
                    // Gọi tới màn hình ADM006 thông báo delete thành công.
                    return RedirectToAction("ADM006", "Susscess", new { MSG = "MSG003" });
                }
            }

                // tra ve man hinh loi
                return RedirectToAction("SystemError", "SystemError", new { MSG = "MSG006" });

            }
            catch(Exception e)
            {
                // tra ve man hinh loi
                return RedirectToAction("SystemError", "SystemError", new { MSG = "MSG006" });
            }


        }
    }
}