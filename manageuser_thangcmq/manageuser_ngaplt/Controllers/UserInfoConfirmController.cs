﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using manageruser.Filter;
using manageruser.Models.Entity;
using manageruser.Models.Logic;
using manageruser.Models.Validates;
using manageruser.Models.Utils;

namespace manageruser.Controllers
{
    /// <summary>
    /// Class confirm userinfo
    /// Creare by NgaPLT 21/12/2019
    /// </summary>
    public class UserInfoConfirmController : Controller
    {
        // GET: UserInfoConfirm
        /// <summary>
        /// Xử lý  hiển thị userinfo
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <returns></returns>
        [LoginFilter]
        public ActionResult ADM004()
        {
            try
            {
                MstGroupLogic mstGroupLogic = new MstGroupLogic();
                MstJapanLogic mstJapanLogic = new MstJapanLogic();
                // lấy list mstgroup
                List<MstGroup> listGroup = new List<MstGroup>();
                listGroup = mstGroupLogic.GetListMstGroup();
                //lấy list mstjapan
                List<MstJapan> listJapan = new List<MstJapan>();
                listJapan = mstJapanLogic.GetListMstJapan();

                UserInfo userInfo = new UserInfo();
                string key = Session["key"].ToString();
                userInfo = (UserInfo)Session[key];
                if(userInfo == null)
                {
                    // tra ve man hinh loi
                    return RedirectToAction("SystemError", "SystemError", new { MSG = "MSG006" });
                }
                foreach (MstGroup group in listGroup)
                {
                    if (group.GroupId == userInfo.GroupId)
                    {
                        userInfo.GroupName = group.GroupName;
                    }
                }
                foreach (MstJapan japan in listJapan)
                {
                    if (japan.CodeLevel == userInfo.CodeLevel)
                    {
                        userInfo.NameCodeLevel = japan.NameLevel;
                    }
                }

                userInfo.Birthday = new DateTime(userInfo.BirthYear, userInfo.BirthMonth, userInfo.BirthDay);
                userInfo.StartDate = new DateTime(userInfo.StartYear, userInfo.StartMonth, userInfo.StartDay);
               
                userInfo.EndDate = new DateTime(userInfo.EndYear, userInfo.EndMonth, userInfo.EndDay);
                ViewBag.key = key;
                ViewBag.userInfo = userInfo;

                return View();
            }catch(Exception e)
            {
                // tra ve man hinh loi
                return RedirectToAction("SystemError", "SystemError", new { MSG = "MSG006" });
            }
        }
        /// <summary>
        /// xử lý add hoăc edit
        /// Create by NgaPLT 21/12/2019
        /// </summary>
        /// <returns></returns>
        [LoginFilter]
        public ActionResult AddEdit()
        {
            try { 
            Boolean result = false;
                Common common = new Common();
            UserValidate userValidate = new UserValidate();
            UserInfo userInfo = new UserInfo();
            string key = Session["key"].ToString();
            userInfo = (UserInfo)Session[key];
            List<string> listErr = new List<string>();
            listErr = userValidate.ValidateUserInfo(userInfo);
            if (listErr.Count == 0)
            {

                    userInfo.Saft = common.randomString();
                    userInfo.Password = common.GetMD5(userInfo.Password + userInfo.Saft);
                    // trường hợp add
                if (userInfo.UserId == 0)
                {
                    result = userValidate.InsertUserInfo(userInfo);
                    if (result)
                    {
                        // Gọi tới màn hình ADM006 thông báo insert thành công.
                        return RedirectToAction("ADM006", "Susscess", new { MSG = "MSG001" });
                    }
                }
                // trường hợp edit
                else 
                {
                        TblUserLogic tblUserLogic = new TblUserLogic();
                        UserInfo user = tblUserLogic.GetUserByUserId(userInfo.UserId);
                        if (user != null)
                        {
                            result = userValidate.UpdateUserInfo(userInfo);
                            if (result)
                            {
                                // Gọi tới màn hình ADM006 thông báo update thành công.
                                return RedirectToAction("ADM006", "Susscess", new { MSG = "MSG002" });
                            }
                        }
                        else
                        {
                            return RedirectToAction("SystemError", "SystemError", new { MSG = "MSG005" });
                        }
                }
            }
                // Trường hợp list lỗi là !=0
                // Di chuyển sang màn hình thông báo lỗi ADM006
                return RedirectToAction("SystemError", "SystemError", new { MSG = "MSG006" });
            }
            catch(Exception e)
            {
                // tra ve man hinh loi
                return RedirectToAction("SystemError", "SystemError", new { MSG = "MSG006" });
            }
        }
    }
}