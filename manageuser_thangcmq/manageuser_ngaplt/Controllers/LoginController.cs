﻿using System;
using System.Web.Mvc;
using manageruser.Models.Entity;
using manageruser.Models.Utils;
using manageruser.Models.Logic;


namespace manageruser.Controllers
{
    /// <summary>
    /// Class xử lý login
    /// Create by NgaPLT 17/12/2019
    /// </summary>
    public class LoginController : Controller
    {
        // GET: Login
        /// <summary>
        /// Hiển thị màn hình ADM001
        /// Create by NgaPLT 17/12/2019
        /// </summary>
        /// <returns> trả về màn hình ADM001</returns>
        public ActionResult ADM001()
        {
            return View();
        }
        /// <summary>
        /// Xử lý login vào hệ thống
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
      [ValidateAntiForgeryToken]
      [ValidateInput(false)]
        public ActionResult Login(TblUser user)
        {
            try
            {
                // Check không nhập loginName và password
                if (string.IsNullOrEmpty(user.LoginName) && string.IsNullOrEmpty(user.Password))
                {
                    ViewBag.ERR01 = ConstansErr.ERR01_LOGIN_NAME;
                    ViewBag.ERR02 = ConstansErr.ERR01_PASS;
                }
                // Check không nhập loginname
                if (string.IsNullOrEmpty(user.LoginName))
                {
                    ViewBag.ERR01 = ConstansErr.ERR01_LOGIN_NAME;
                }
                // Check không nhập password
                else if (string.IsNullOrEmpty(user.Password))
                {
                    ViewBag.ERR02 = ConstansErr.ERR01_PASS;
                }
                else
                {
                    TblUserLogic tblUserLogic = new TblUserLogic();
                    // Check sự tồn tại của loginname. password
                    Boolean checkLogin = tblUserLogic.CheckLogin(user.LoginName, user.Password);
                    if (checkLogin)
                    {
                        // Trường hợp không có lỗi chuyển sang ADM002 và lưu login name lên session
                        Session["LoginName"] = user.LoginName;
                        Session["pass"] = user.Password;
                        return RedirectToAction("ADM002", "ListUser");
                    }
                    // Có lỗi , user không tồn tại hoặc không được quyền truy cập
                    ViewBag.ERR01 = ConstansErr.ERRR16;


                }

                // View ADM001
                return View("ADM001");
            }
            catch (Exception e)
            {
                // Di chuyển đến màn hình thông báo lỗi.
                return RedirectToAction("SystemError", "SystemError", new { MSG = "MSG006" });
            }

        }
    }
}