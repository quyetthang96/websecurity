﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using manageruser.Filter;
using manageruser.Models.Logic;
using manageruser.Models.Entity;
using manageruser.Models.Utils;

namespace manageruser.Controllers
{
    /// <summary>
    /// Class xử lý hiển thị listUser, search, sort, paging
    /// Create by NgaPLT 18/12/2019
    /// </summary>
    public class ListUserController : Controller
    {
        // GET: ListUser
        /// <summary>
        /// Xử lý hiển thị listUser, search, sort, paging
        /// Create by NgaPLT 18/12/2019
        /// </summary>
        /// <returns></returns>
        [LoginFilter]

        public ActionResult ADM002()
        {
            try
            {
                //Khai báo tblUserLogic, mstGroupLogic
                TblUserLogic tblUserLogic = new TblUserLogic();
                MstGroupLogic mstGroupLogic = new MstGroupLogic();
                // Khai báo các giá trị default
                string sortByFullName = ConstansFormat.ASC;
                string sortByCodeLevel = ConstansFormat.ASC;
                string sortByEndDate = ConstansFormat.DESC;
                int groupId = 0;
                string fullName = null;
                int limit = ConstansFormat.LIMIT;
                int currenPage = 1;
                string sortType = "full_name";
                int totalPage = 0;
                string action = Request.QueryString["type"];
                // nếu là các trường hợp ngoài default
                if (action != null)
                {
                    // trường hợp back, lấy các giá trị đã lưu trên session
                    if (action == "back")
                    {
                        fullName = (string)Session["fullName"];
                        groupId = (int)Session["groupId"];
                        sortByFullName = (string)Session["sortByFullName"];
                        sortByCodeLevel = (string)Session["sortByCodeLevel"];
                        sortByEndDate = (string)Session["sortByEndDate"];
                        sortType = (string)Session["sortType"];
                        currenPage = (int)Session["currenPage"];
                    }
                    // trường hợp search, sort, paging
                    else if (action.Equals("sort") || action.Equals("search") || action.Equals("paging"))
                    {
                        fullName = Request.Unvalidated.QueryString["fullName"];
                        groupId = Common.ParseInt(Request.Unvalidated.QueryString["groupId"]);
                        // sort
                        if (action == "sort")
                        {
                            sortType = Request.QueryString["sortType"];
                            // sortType là fullName
                            if (sortType == "full_name")
                            {
                                sortByFullName = Request.QueryString["sortByFullName"];
                            }
                            // sortType là codelevel
                            if (sortType == "code_level")
                            {
                                sortByCodeLevel = Request.QueryString["sortByCodeLevel"];
                            }
                            // sortType là endDate
                            if (sortType == "end_date")
                            {
                                sortByEndDate = Request.QueryString["sortByEndDate"];
                            }

                        }
                        // paging
                        else if (action == "paging")
                        {
                            sortType = Request.QueryString["sortType"];
                            sortByFullName = Request.QueryString["sortByFullName"];
                            sortByCodeLevel = Request.QueryString["sortByCodeLevel"];
                            sortByEndDate = Request.QueryString["sortByEndDate"];
                            currenPage = int.Parse(Request.QueryString["currentPage"]);
                        }
                    }
                }
                // nếu các giá trị sortByFullName, sortByCodeLevel, sortByEndDate thì gán lại về giá trị mặc định
                if (!sortByFullName.Equals(ConstansFormat.ASC) && !sortByFullName.Equals(ConstansFormat.DESC))
                {
                    sortByFullName = ConstansFormat.ASC;
                }
                if (!sortByCodeLevel.Equals(ConstansFormat.ASC) && !sortByCodeLevel.Equals(ConstansFormat.DESC))
                {
                    sortByCodeLevel = ConstansFormat.ASC;
                }
                if (!sortByEndDate.Equals(ConstansFormat.ASC) && !sortByEndDate.Equals(ConstansFormat.DESC))
                {
                    sortByEndDate = ConstansFormat.DESC;
                }
                if (!sortType.Equals("full_name") && !sortType.Equals("code_level") && !sortType.Equals("end_date"))
                {
                    sortType = "full_name";
                }

                //tính totalPage
                totalPage = Common.GetTotalPage(tblUserLogic.GetTotalUser(fullName, groupId), limit);
              
                // nếu currentPage >totalPage , gán bằng totalPage
                if (currenPage > totalPage)
                {
                    currenPage = totalPage;
                }
                // nếu currentPage < 1 gán về lại bằng 1
                if (currenPage < 1)
                {
                    currenPage = 1;
                }
                int offSet = Common.GetOffSet(limit, currenPage);
                // lấy list mstgroup
                List<MstGroup> listGroup = new List<MstGroup>();
                listGroup = mstGroupLogic.GetListMstGroup();

                // lấy listUser theo điều kiện
                List<UserInfo> listUserInfo = new List<UserInfo>();
                listUserInfo = tblUserLogic.GetListUser(offSet, limit, groupId, fullName, sortType, sortByFullName, sortByCodeLevel, sortByEndDate);

                List<int> listPaging = new List<int>();
                listPaging = Common.GetListPaging(totalPage, limit, currenPage);




                //Truyền qua view
                Session["fullName"] = fullName;
                Session["groupId"] = groupId;
                Session["sortByFullName"] = sortByFullName;
                Session["sortByCodeLevel"] = sortByCodeLevel;
                Session["sortByEndDate"] = sortByEndDate;
                Session["sortType"] = sortType;
                Session["currenPage"] = currenPage;

                ViewBag.listGroup = listGroup;
                ViewBag.listuserInfo = listUserInfo;
                ViewBag.listPaging = listPaging;
                ViewBag.totalPage = totalPage;
                return View();
            }catch(Exception e)
            {
                // tra ve man hinh loi
                return RedirectToAction("SystemError", "SystemError", new { MSG = "MSG006" });
            }
        }
    }
}